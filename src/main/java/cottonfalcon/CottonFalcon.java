package cottonfalcon;

//import java.util.List;
import java.util.LinkedList;

/**
 * CottonFalcon class parses GNU-style command line options.
 *
 *  Examples of options:
 * <ul>
 * <li> -s          -- only one letter can follow a "-" (short option)
 * <li> -s word     -- short options  can have parameter
 * <li> --long      -- There are long options
 * <li> --long bar  -- Long options also can have a parameter,
 * </ul>
 * Some notes about implementation:
 * <ul>
 * <li> "--long=bar" is not allowed
 * <li> each option can be specified only once
 * <li> "short" options are exactly one letter, "long" options must have more
 * than two letters
 * </ul>
 * @version     %I%, %G%
 * @since       0.1
 */
public class CottonFalcon {

  protected LinkedList<String> shortOptions;
  protected LinkedList<String> longOptions;
  protected LinkedList<String> optionsWithParameters;

  LinkedList<OptionInfo> foundOptions;
  LinkedList<String> arguments;

  protected String errorMessage;

  public CottonFalcon() {
    shortOptions = new LinkedList<String>();
    longOptions = new LinkedList<String>();
    optionsWithParameters = new LinkedList<String>();

    foundOptions = new LinkedList<OptionInfo>();
    arguments = new LinkedList<String>();

    errorMessage = "";
  }

  protected static final String emLongShortOption =
      "Short option must be exactly one char length";
  protected static final String emShortLongOption =
      "Long option key must be at least two characters";
  protected static final String emBothOptionKeysEmpty =
      "Both option keys can't be empty";

  public boolean hasError() {
    return !errorMessage.isEmpty();
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  /**
   *   Adds command line option to search for.
   *   Either shortOption or longOption can be empty string or null (but not
   *  both)
   * <p>
   *   By default it supposes there is no parameter for this option
   *
   * @param shortOption  A string key of the short option. Can be empty string
   *                     if this option has only long key. Must be exactly one
   *                     char length, the method will fail otherwise. Or empty
   *                     string if the option has long key only
   * @param longOption   A string key of the short option. Must be at least two
   *                     characters length, or empty string if option has
   *                     a short key only.
   * @param hasParameter Should be true if option will be followed by parameter
   *
   * @return             true on success, false if something is wrong
   *
   * @see             #addLongOption
   * @see             #addShortOption
   * @since           0.1
   */
  public boolean addOption(String shortOption, String longOption,
                           boolean hasParameter) {
    if ( shortOption.isEmpty() && longOption.isEmpty() ) {
      errorMessage = emBothOptionKeysEmpty;
      return false;
    }

    if ((!shortOption.isEmpty()) && (shortOption.length()>1)) {
      errorMessage = emLongShortOption;
      return false;
    }

    if ((!longOption.isEmpty()) && (longOption.length()<2)) {
      errorMessage = emShortLongOption;
      return false;
    }
    //Now all tests are done

    if(!shortOption.isEmpty()) {
      shortOptions.add(shortOption);
      if (hasParameter) {
        optionsWithParameters.add(shortOption);
      }
    }

    if(!longOption.isEmpty()) {
      longOptions.add(longOption);
      if (hasParameter) {
        optionsWithParameters.add(longOption);
      }
    }

    return true;
  }

  /**
   *   Adds long command line option to search for.
   *   Similar to addOption, but for long options (should be called if option
   * has only long form)
   * <p>
   *   By default it supposes there is no parameter for this option
   *
   * @param option       A string key of the option. Must be at least two
   *                     characters length
   * @param hasParameter Should be true if option will be followed by parameter
   *
   * @return             true on success, false if something is wrong
   *
   * @see             #addOption
   * @see             #addShortOption
   * @since           0.1
   */
  public boolean addLongOption(String option, boolean hasParameter) {
    return addOption("",option, hasParameter);
  }

  /**
   *   Adds long command line option to search for.
   *   Similar to addOption, but for long options (should be called if option
   * has only long form)
   * <p>
   *   By default it supposes there is no parameter for this option
   *
   * @param option       A string key of the short option. Must be exactly one
   *                     character length.
   * @param hasParameter Should be true if option will be followed by parameter
   *
   * @return             true on success, false if something is wrong
   *
   * @see             #addOption
   * @see             #addLongOption
   * @since           0.1
   */
  public boolean addShortOption(String option, boolean hasParameter) {
    return addOption(option,"", hasParameter);
  }

  /**
   *   Processes array of command line arguments.
   *
   * @param args   Array of command line arguments, like in "public void main"
   *
   * @return             true on success, false if something is wrong
   *
   * @since           0.1
   */
  public boolean process(String[] args) {

    BasicProcessor processor = new BasicProcessor() ;
    processor.setShortOptions(shortOptions);
    processor.setLongOptions(longOptions);
    processor.setParametrisedOptions(optionsWithParameters);

    boolean result = processor.process(args);
    if (!result) {
      errorMessage = processor.getErrorMessage();
    }

    foundOptions = processor.getOptions();
    arguments = processor.getOtherValues();

    return result;
  }

  /**
   *   Checks if option was specified among command line arguments.
   *
   *   There will be no error if option was not registered before.
   *
   * @param shortOption  A string key of the short option. Can be empty string
   *                     if this option has only long key.
   * @param longOption   A string key of the long option. Can be empty string
   *                     if this option has only short key.
   *
   * @return        true if option was provided with arguments, false otherwise
   *
   * @see             #gotShortOption
   * @see             #gotLongOption
   *
   * @since           0.1
   */
  public boolean gotOption(String shortOption, String longOption) {

    for (OptionInfo oinfo:foundOptions) {
      //System.out.println("CF.gO: working") ;

      if ((!shortOption.isEmpty()) && shortOption.equals(oinfo.shortKey)) {
        return true;
      }

      if ((!longOption.isEmpty()) && longOption.equals(oinfo.longKey)) {
        return true;
      }
    }

    return false;
  }

  /**
   *   Checks if short option was specified among command line arguments.
   *
   *   There will be no error if option was not registered before.
   *
   * @param option  A string key of the option.
   *
   * @return        true if option was prowided with arguments, false otherwise
   *
   * @see             #gotOption
   * @see             #gotLongOption
   *
   * @since           0.1
   */
  public boolean gotShortOption(String option) {
    return gotOption(option, "");
  }

  /**
   *   Checks if a long option was specified among command line arguments.
   *
   *   There will be no error if option was not registered before.
   *
   * @param option  A string key of the option.
   *
   * @return        true if option was prowided with arguments, false otherwise
   *
   * @see             #gotOption
   * @see             #gotLongOption
   *
   * @since           0.1
   */
  public boolean gotLongOption(String option) {
    return gotOption("", option);
  }

  /**
   *   Returns parameter given for the option.
   *
   *   There will be no error if option was not registered before.
   *
   * @param shortOption  A string key of the short option. Can be empty string
   *                     if this option has only long key.
   * @param longOption   A string key of the long option. Can be empty string
   *                     if this option has only short key.
   *
   * @return        The parameter as a string if option was processed,
   *                empty string in other cases
   *
   * @see             #getShortOptionParameter
   * @see             #getLongOptionParameter
   *
   * @since           0.1
   */
  public String getOptionParameter(String shortOption, String longOption) {

    if ( shortOption.isEmpty() && longOption.isEmpty() ) {
      return null;
    }

    for (OptionInfo oinfo:foundOptions) {
      if ((!shortOption.isEmpty()) && shortOption.equals(oinfo.shortKey)) {
        return oinfo.param;
      }

      if ((!longOption.isEmpty()) && longOption.equals(oinfo.longKey)) {
        return oinfo.param;
      }
    }

    return "";
  }

  /**
   *   Returns parameter given for the option.
   *
   *   There will be no error if option was not registered before.
   *
   * @param option  A string key of the short option.
   *
   * @return        The parameter as a string if option was processed, null otherwise
   *
   * @see             #getOptionParameter
   * @see             #getLongOptionParameter
   *
   * @since           0.1
   */
  public String getShortOptionParameter(String option) {
    return getOptionParameter(option, "");
  }

  /**
   *   Returns parameter given for the option.
   *
   *   There will be no error if option was not registered before.
   *
   * @param option   A string key of the long option.
   *
   * @return        The parameter as a string if option was processed, null otherwise
   *
   * @see             #getShortOptionParameter
   * @see             #getOptionParameter
   *
   * @since           0.1
   */
  public String getLongOptionParameter(String option) {
    return getOptionParameter("", option);
  }

  /**
   *   Returns arguments (parts of the command line which are neither options
   *  nor option parameters).
   *
   * @return     array of string items
   *
   * @since           0.1
   */
  public String[] getArguments() {
    String[] result = arguments.toArray(new String[0]);
    return result;
  }
}

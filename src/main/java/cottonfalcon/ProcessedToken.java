package cottonfalcon;

//import java.util.ArrayList;

class ProcessedToken {
  TokenType type;
  String value;

  private ProcessedToken () {
    type = TokenType.ERROR;
    value = "";
  }

  public ProcessedToken (String arg) {
    type = TokenType.ERROR;
    value = "";

    setValue(arg);
  }

  protected boolean setValue(String val) {
    type = TokenType.ERROR;
    value = "";

    if (val.isEmpty()) {
      return false;
    }

    if (val.startsWith("--")) {
      type = TokenType.LONGKEY;
      value = val.substring(2) ;
      if (value.length()<2) { //long option must be more than two symbols
        type = TokenType.ERROR;
        value = "";
      }

      return true;
    }

    if (val.charAt(0)=='-') {
      type = TokenType.SHORTKEY ;
      value = val.substring(1);
      if (value.length()>1) {
        type = TokenType.ERROR;
        value = "";
      }
      return true;
    }

    // else
    type = TokenType.VALUE ;
    value = val;
    return true;
  }

  public TokenType getType() {
    return type;
  }

  public String getValue() {
    return value;
  }
}
package cottonfalcon;

import java.util.ArrayList;

class ProcessedTokenKeeper {

//  String errorMessage;
  ArrayList<ProcessedToken> tokens;
  int index = 0;

  public ProcessedTokenKeeper () {
    tokens = new ArrayList<ProcessedToken>();
    index = 0;
  }

  public boolean loadTokens(String[] args) {
    tokens.clear();
    for (int ai=0; ai<args.length; ai++) {
      //System.out.println("CF.ptk :processing " + args[ai]);

      ProcessedToken pt = new ProcessedToken(args[ai]) ;
      if (pt.getType() != TokenType.ERROR) {
        tokens.add(pt) ;
      }
      else {
        //System.out.println("CF.ptk: incorrect token " + args[ai]);
        return false;
      }
    }

    index = 0;

    //System.out.println(String.format("CF.ptk: loaded %d tokens", tokens.size()));

    return true;
  }

  public ProcessedToken nextToken() {
    int nextIndex = index+1;
    if (nextIndex>=tokens.size()) {
      return null;
    }

    index = nextIndex;
    return tokens.get(nextIndex) ;
  }

  public ProcessedToken currentToken() {
    if (index>=tokens.size()) {
      return null;
    }
    else {
      //System.out.println(String.format("CF.ptk: returning token #%d(of %d)", index, tokens.size()));
      return tokens.get(index);
    }
  }

  public boolean hasMoreTokens() {
    int nextIndex = index+1;
    if (nextIndex>=tokens.size()) {
      return false;
    }
    else {
      return true;
    }
  }

}

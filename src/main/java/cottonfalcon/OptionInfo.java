package cottonfalcon;

class OptionInfo {
  public String shortKey;
  public String longKey;
  public String param;

  public OptionInfo() {
    shortKey="";
    longKey = "";
    param = "";
  }
}
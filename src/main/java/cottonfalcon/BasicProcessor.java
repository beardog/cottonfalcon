package cottonfalcon;

import java.util.LinkedList;

public class BasicProcessor {

  protected LinkedList<String> shortOptions;
  protected LinkedList<String> longOptions;
  protected LinkedList<String> optionsWithParameters;

  protected String errorMessage;

  public BasicProcessor() {
    shortOptions = new LinkedList<String>();
    longOptions = new LinkedList<String>();
    optionsWithParameters = new LinkedList<String>();

    foundOptions = new LinkedList<OptionInfo>();
    otherValues = new LinkedList<String>();

    errorMessage = "";
  }

  public void setShortOptions(LinkedList<String> list){
    shortOptions = list;
  }

  public void setLongOptions(LinkedList<String> list){
    longOptions = list;
  }

  public void setParametrisedOptions(LinkedList<String> list){
    optionsWithParameters = list;
  }

  protected static final String emOptionMixedWithArg =
          "Options must appear before other arguments";
  protected static final String emInternal01 =
          "Internal logic error (stopper)";
  protected static final String emUnregisteredOption = "Unknown option";
  protected static final  String emFailedToGetParameter =
          "Failed to get required parameter for an option" ;

  public boolean hasError() {
    return !errorMessage.isEmpty();
  }

  public String getErrorMessage() {
    return errorMessage;
  }

  LinkedList<OptionInfo> foundOptions;
  protected LinkedList<OptionInfo> getOptions(){
    return foundOptions;
  }

  LinkedList<String> otherValues;
  protected LinkedList<String> getOtherValues() {
    return otherValues;
  }

  enum ProcessingStage{
      START
    , READING_ARG
    , ERROR
    , CHECK_OPTION
    , READ_PARAMETER
    , GET_NEXT
    , FINISHED
  }

  class ProcessingStageInfo {
    ProcessingStage stage;
    ProcessedToken token;
    OptionInfo currentOption;
  }



  public boolean process(String[] args) {
    //System.out.println("CF.bp: processing arguments");

    ProcessedTokenKeeper ptk = new ProcessedTokenKeeper();
    ptk.loadTokens(args);

    foundOptions = new LinkedList<OptionInfo>();
    otherValues = new LinkedList<String>();

    ProcessingStageInfo psInfo = new ProcessingStageInfo();
    psInfo.stage = ProcessingStage.START;
    boolean stop = false;
    int stopper = 0;
    final int stopperTh = 1000;
    while (!stop) {
      switch (psInfo.stage) {
        case START:
          psInfo = pssStart(psInfo, ptk);
          break;

        case READING_ARG:
          psInfo = pssReadingArg(psInfo, ptk);
          break;

        case CHECK_OPTION:
          psInfo = pssCheckOption(psInfo, ptk);
          break;

        case READ_PARAMETER:
          psInfo = pssReadParameter(psInfo, ptk);
          break;

        case GET_NEXT:
          psInfo = pssGetNext(psInfo, ptk);
          break;

        case ERROR:
        case FINISHED:
          stop = true;
      }

      stopper++;
      if (stopper>stopperTh) {
        stop=true;
      }
    }

    if (psInfo.stage == ProcessingStage.ERROR) {
      return false ;
    }

    if (stopper > stopperTh) {
      errorMessage = emInternal01;
      //System.out.println("CF.bp: stopper triggered") ;
      return false;
    }

    return true;
  }

  ProcessingStageInfo pssStart(ProcessingStageInfo stageInfo,
                               ProcessedTokenKeeper ptk) {
    //
    ProcessedToken token = ptk.currentToken();
    if (token==null) {
      //System.out.println("CF.pssStart: no arguments at all") ;
      stageInfo.stage= ProcessingStage.FINISHED;
      return stageInfo ;
    }

    if (token.getType()==TokenType.VALUE) {
      //showStandardStreams.println("CF.pssStart: first token is argument (no clo at all)") ;
      otherValues.add(token.getValue());
      stageInfo.stage = ProcessingStage.READING_ARG;
    }
    else {
      //System.out.println("clop: first token is like option: " + token.getValue()) ;
      stageInfo.stage = ProcessingStage.CHECK_OPTION;
    }

    stageInfo.token = token;

    return stageInfo;
  }

  ProcessingStageInfo pssReadingArg(ProcessingStageInfo stageInfo,
                                    ProcessedTokenKeeper ptk) {
    if (ptk.hasMoreTokens()) {
      ProcessedToken tk = ptk.nextToken();
      if (tk.getType()==TokenType.VALUE) {
        stageInfo.stage = ProcessingStage.READING_ARG;//still the same
        otherValues.add(tk.getValue());
      }
      else {
        stageInfo.stage = ProcessingStage.ERROR;
        errorMessage = emOptionMixedWithArg;
      }
    }
    else {
      stageInfo.stage= ProcessingStage.FINISHED;
    }
    return stageInfo;
  }

  ProcessingStageInfo pssCheckOption(ProcessingStageInfo stageInfo,
                                    ProcessedTokenKeeper ptk) {
    // First, check if this option was registered (is known)
    boolean found = false;
    if (stageInfo.token.getType()==TokenType.SHORTKEY) {
      for (String shos: shortOptions) {
        //System.out.println("CF.pssCO: compare " + shos + " with short " + stageInfo.token.getValue()) ;
        if(shos.equals(stageInfo.token.getValue())) {
          found = true;
        }
      }
    }
    else if (stageInfo.token.getType()==TokenType.LONGKEY) {
      for (String los: longOptions) {
        //System.out.println("CF.pssCO: compare " + los + " with long" + stageInfo.token.getValue()) ;
        if(los.equals(stageInfo.token.getValue())) {
          found = true;
        }
      }
    }

    if (!found) {
      stageInfo.stage = ProcessingStage.ERROR;
      errorMessage = emUnregisteredOption;
      return stageInfo;
    }

    // Prepare current option (it will be used anyway)
    OptionInfo oinfo = new OptionInfo();
    if (stageInfo.token.getType()==TokenType.SHORTKEY) {
      oinfo.shortKey = stageInfo.token.getValue();
    }
    else {
      oinfo.longKey = stageInfo.token.getValue();
    }

    // Check for parameter
    boolean requiresParameter = false;
    for (String pos: optionsWithParameters) {
      if(pos.equals(stageInfo.token.getValue())) {
        requiresParameter = true;
      }
    }

    if(requiresParameter) {
      //System.out.println("CF.pssCO: option requires parameter " ) ;
      stageInfo.stage = ProcessingStage.READ_PARAMETER ;
      stageInfo.currentOption = oinfo; //so we keep option key for further processing
    }
    else {
      //System.out.println(
      //          String.format("CF.pssCO: keep option '%s' without parameter ",
      //                        stageInfo.token.getValue())) ;
      foundOptions.add(oinfo);

      stageInfo.stage = ProcessingStage.GET_NEXT;
    }

    return stageInfo;
  }

  ProcessingStageInfo pssReadParameter(ProcessingStageInfo stageInfo,
                                       ProcessedTokenKeeper ptk) {
    if (ptk.hasMoreTokens()) {
      ProcessedToken tk = ptk.nextToken();
      if (tk.getType()==TokenType.VALUE) {
        stageInfo.stage = ProcessingStage.GET_NEXT;
        //System.out.println("CF.pssRP: keep option '"+ tk.getValue()+ "' with parameter " ) ;

        OptionInfo oinfo = stageInfo.currentOption;
        oinfo.param = tk.getValue();
        foundOptions.add(oinfo);

        stageInfo.currentOption = new OptionInfo();

        stageInfo.stage = ProcessingStage.GET_NEXT;
      }
      else {
        stageInfo.stage = ProcessingStage.ERROR;
        errorMessage = emFailedToGetParameter;
      }
    }
    else {
      stageInfo.stage= ProcessingStage.ERROR;
      errorMessage = emFailedToGetParameter;
    }
    return stageInfo;
  }

  ProcessingStageInfo pssGetNext(ProcessingStageInfo stageInfo,
                                 ProcessedTokenKeeper ptk) {
    if (ptk.hasMoreTokens()) {
      ProcessedToken tk = ptk.nextToken();
      if (tk.getType()==TokenType.VALUE) {
        stageInfo.stage = ProcessingStage.READING_ARG;
        otherValues.add(tk.getValue());
      }
      else {
        stageInfo.stage = ProcessingStage.CHECK_OPTION;
        stageInfo.token = tk ;
      }
    }
    else {
      stageInfo.stage= ProcessingStage.FINISHED;
    }

    return stageInfo;
  }

}

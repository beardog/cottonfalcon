package debugApp;

import cottonfalcon.CottonFalcon;

public class MainApp {



  public static void main(String[] args) {
    System.out.println("application started"); //


    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addOption("h", "help", false);
    if (!aor) {
      System.out.println("Error: failed to add option \"help\"" ) ;
      System.out.println("error message was" + cf.getErrorMessage() ) ;
    }
    aor = cf.addLongOption("db", true);
    if (!aor) {
      System.out.println("Error: failed to add option \"db\"" ) ;
      System.out.println("error message was" + cf.getErrorMessage() ) ;
    }
    aor = cf.addLongOption("json", true);
    if (!aor) {
      System.out.println("Error: failed to add option \"json\"" ) ;
      System.out.println("error message was" + cf.getErrorMessage() ) ;
    }


    final String logLevelShCO = "l";
    final String logLevelLCO = "loglevel";
    aor = cf.addOption(logLevelShCO, logLevelLCO, true);
    if (!aor) {
      System.out.println("Error: failed to add option \"loglevel\"" ) ;
      System.out.println("error message was" + cf.getErrorMessage() ) ;
    }

//    String[] arr = {"l", "INFO",
//                    "db", "./public.sqlite",
//                    "json", "long.file.name.here.json"};
    boolean pres = cf.process(args);
    if (!pres) {
      System.out.println("Error (pres):" + cf.getErrorMessage()) ;
      return;
    }

    if (cf.hasError()) {
      System.out.println("Error (he):" + cf.getErrorMessage()) ;
    }

    boolean tboo = cf.gotLongOption("db");
    if (tboo==false) {
      System.out.println("There was no \"db\" option here" );
    } else {
      System.out.println("Got \"db\" option with param "
                         + cf.getOptionParameter("","db") );
    }


    tboo = cf.gotLongOption("json");
    if (tboo) {
      System.out.println("Got \"json\" option with param "
                         + cf.getOptionParameter("","json") );
    } else {
      System.out.println("There was no \"json\" option here" );
    }

    System.out.println("application finished."); //
  }
}

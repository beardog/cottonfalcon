package cottonfalcon;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class CottonFalconTest {

  // ==========================================================================

  @Test
  public void testAddOption() {
    CottonFalcon cf = new CottonFalcon();
    boolean aor = cf.addOption("a", "alfa", false);
    assertEquals(true, aor) ;
  }

  @Test
  // short option string (with one "-") must be only one char
  public void testAddOptionShSh() {
    CottonFalcon cf = new CottonFalcon();
    boolean aor = cf.addOption("bbb", "bravo", false);
    assertEquals(false, aor) ;
    String em = cf.getErrorMessage();
    assertEquals(em, CottonFalcon.emLongShortOption);
  }

  @Test
  public void testAddOptionBothEmpty() {
    CottonFalcon cf = new CottonFalcon();
    boolean aor = cf.addOption("", "", false);
    assertEquals(false, aor) ;
    assertEquals(cf.hasError(), true);
    assertEquals(cf.getErrorMessage(), CottonFalcon.emBothOptionKeysEmpty);
  }

  // ==========================================================================

  @Test
  // short option string (with one "-") must be only one char
  public void testAddShortOptionSh() {
    CottonFalcon cf = new CottonFalcon();
    boolean aor = cf.addShortOption("bbb", false);
    assertEquals(false, aor) ;
    String em = cf.getErrorMessage();
    assertEquals(em, CottonFalcon.emLongShortOption);
  }

  // ==========================================================================

  @Test
  // short option string (with one "-") must be only one char
  public void testAddLongOptionSh() {
    CottonFalcon cf = new CottonFalcon();
    boolean aor = cf.addLongOption("a", false);
    assertEquals(false, aor) ;
    String em = cf.getErrorMessage();
    assertEquals(em, CottonFalcon.emShortLongOption);
  }

  // ==========================================================================

  @Test
  public void testGotOption() {
    CottonFalcon cf = new CottonFalcon();
    boolean aor = cf.gotOption("a", "alfa");
    assertEquals(false, aor) ;
  }

  @Test
  public void testGotOptionBothEmpty() {
    CottonFalcon cf = new CottonFalcon();
    boolean aor = cf.gotOption("", "");
    assertEquals(false, aor) ;// actually we check if no exception happens here
  }

  @Test
  public void testGotShortOption() {
    CottonFalcon cf = new CottonFalcon();
    boolean aor = cf.gotShortOption("b");
    assertEquals(false, aor) ;
  }

  @Test
  public void testGotLongOption() {
    CottonFalcon cf = new CottonFalcon();
    boolean aor = cf.gotLongOption("ff");
    assertEquals(false, aor) ;
  }

  // ==========================================================================

  @Test
  public void testProcessBasic() {
    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addShortOption("a", false);
    assertEquals(aor, true);
    aor = cf.addLongOption("ff", false);
    assertEquals(aor, true);
    String[] arr = {"-a", "--ff", "boobs"};
    boolean pres = cf.process(arr);
    assertEquals(true, pres) ;

    assertEquals(cf.getLongOptionParameter("dd"), "");
    assertEquals(cf.getShortOptionParameter("d"), "");
    assertEquals(cf.getOptionParameter("dd", "d"), "");
  }

  @Test
  public void testProcess1s1p0l() {
    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addShortOption("a", true);
    assertEquals(aor, true);
    String[] arr = {"-a", "pax"};
    boolean pres = cf.process(arr);
    assertEquals(true, pres) ;

    assertEquals(cf.getLongOptionParameter("dd"), "");
    assertEquals(cf.getShortOptionParameter("a"), "pax");

    String[] otherArgs = cf.getArguments();
    assertEquals(otherArgs.length, 0);
  }

  @Test
  public void testProcess1s1p0l1o() {
    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addShortOption("a", true);
    assertEquals(aor, true);
    String[] arr = {"-a", "pax", "foobar"};
    boolean pres = cf.process(arr);
    assertEquals(true, pres) ;

    assertEquals(cf.getLongOptionParameter("dd"), "");
    assertEquals(cf.getShortOptionParameter("a"), "pax");

    String[] otherArgs = cf.getArguments();
    assertEquals(otherArgs.length, 1);
    assertEquals(otherArgs[0], "foobar");
  }

  @Test
  public void testProcess0s1l1p() {
    // testing 0 short, 1 long with param, 0 additional
    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addLongOption("dark", true);
    assertEquals(aor, true);
    String[] arr = {"--dark", "pax"};
    boolean pres = cf.process(arr);
    assertEquals(true, pres) ;

    assertEquals(cf.getLongOptionParameter("dd"), "");
    assertEquals(cf.getLongOptionParameter("dark"), "pax");

    String[] otherArgs = cf.getArguments();
    assertEquals(otherArgs.length, 0);
  }

  @Test
  public void testProcess0s1l1p1o() {
    // testing 0 short, 1 long with param, 1 additional
    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addLongOption("white", true);
    assertEquals(aor, true);
    String[] arr = {"--white", "pax", "foobar"};
    boolean pres = cf.process(arr);
    assertEquals(true, pres) ;

    assertEquals(cf.getLongOptionParameter("dd"), "");
    assertEquals(cf.getLongOptionParameter("white"), "pax");

    String[] otherArgs = cf.getArguments();
    assertEquals(otherArgs.length, 1);
    assertEquals(otherArgs[0], "foobar");
  }

  @Test
  public void testProcess0s2l1p1o() {
    // testing 0 short, 2 long  (1 with param), 1 additional
    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addLongOption("white", true);
    aor = cf.addLongOption("blue", false);
    assertEquals(aor, true);
    String[] arr = {"--white", "pax", "foobar"};
    boolean pres = cf.process(arr);
    assertEquals(true, pres) ;

    assertEquals(cf.getLongOptionParameter("dd"), "");
    assertEquals(cf.getLongOptionParameter("white"), "pax");

    String[] otherArgs = cf.getArguments();
    assertEquals(otherArgs.length, 1);
    assertEquals(otherArgs[0], "foobar");
  }

  @Test
  public void testProcess0s1l1p0oE1() {
    // testing 0 short, 2 long  (1 with param), 1 additional
    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addLongOption("white", true);
    assertEquals(aor, true);
    String[] arr = {"--white"}; // incorrect line, option requires a parameter
    boolean pres = cf.process(arr);
    assertEquals(false, pres) ;
    assertEquals(cf.getErrorMessage(), BasicProcessor.emFailedToGetParameter);

  }

  @Test
  public void testProcess1s1pE1() {
    // testing 1 short with param, 0 long
    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addOption("w","", true);
    assertEquals(aor, true);
    String[] arr = {"-w"}; // incorrect line, option requires a parameter
    boolean pres = cf.process(arr);
    assertEquals(false, pres) ;
    assertEquals(cf.getErrorMessage(), BasicProcessor.emFailedToGetParameter);
  }

  @Test
  public void testProcess1s0pE1() {
    // testing 1 short , 0 long
    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addOption("w","", false);
    assertEquals(aor, true);
    String[] arr = {"-w", "-u"}; // incorrect line, unregistered option
    boolean pres = cf.process(arr);
    assertEquals(false, pres) ;
    assertEquals(cf.getErrorMessage(), BasicProcessor.emUnregisteredOption);
  }

  @Test
  public void testProcess1s0pE2() {
    // testing 1 short , 0 long
    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addOption("w","", false);
    assertEquals(aor, true);
    String[] arr = {"ffffff", "-w", "ffooasa"}; // incorrect line, option mixed
    boolean pres = cf.process(arr);
    assertEquals(false, pres) ;
    assertEquals(cf.getErrorMessage(), BasicProcessor.emOptionMixedWithArg);
  }

  @Test
  public void testProcess2s0p() {
    // testing 2 short without param
    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addShortOption("a", false);
    assertEquals(aor, true);
    aor = cf.addShortOption("b", false);
    assertEquals(aor, true);

    String[] arr = {"-b", "-a"};
    boolean pres = cf.process(arr);
    assertEquals(true, pres) ;


    assertEquals(cf.gotShortOption("a"), true);
    assertEquals(cf.gotShortOption("b"), true);

    String[] otherArgs = cf.getArguments();
    assertEquals(otherArgs.length, 0);
  }

  @Test
  public void testProcess2l0p() {
    // testing 2 long without param
    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addLongOption("aaa", false);
    assertEquals(aor, true);
    aor = cf.addLongOption("bbb", false);
    assertEquals(aor, true);

    String[] arr = {"--aaa", "--bbb"};
    boolean pres = cf.process(arr);
    assertEquals(true, pres) ;


    assertEquals(cf.gotLongOption("aaa"), true);
    assertEquals(cf.gotLongOption("bbb"), true);

    String[] otherArgs = cf.getArguments();
    assertEquals(otherArgs.length, 0);
  }

  @Test
  public void testProcess2a() {
    // testing no cmd line options set, 2 args in command line
    CottonFalcon cf = new CottonFalcon();

    String[] arr = {"aaa", "sfsf"};
    boolean pres = cf.process(arr);
    assertEquals(true, pres) ;

    String[] otherArgs = cf.getArguments();
    assertEquals(otherArgs.length, 2);
    assertEquals(otherArgs[0], "aaa");
    assertEquals(otherArgs[1], "sfsf");
  }

  @Test
  public void testProcessWithNoArgs() {
    // testing empty cmd line (no parameters ot options)
    CottonFalcon cf = new CottonFalcon();

    String[] arr = {};
    boolean pres = cf.process(arr);
    assertEquals(true, pres) ;

    String[] otherArgs = cf.getArguments();
    assertEquals(otherArgs.length, 0);
  }

  @Test
  public void testProcessIssueN2A() {
    // testing 2 long without param
    CottonFalcon cf = new CottonFalcon();
    boolean aor ;
    aor = cf.addOption("h", "help", false);
    assertEquals(aor, true);
    aor = cf.addLongOption("db", true);
    assertEquals(aor, true);
    aor = cf.addLongOption("json", true);
    assertEquals(aor, true);

    final String logLevelShCO = "l";
    final String logLevelLCO = "loglevel";
    aor = cf.addOption(logLevelShCO, logLevelLCO, true);
    assertEquals(aor, true);

    String[] arr = {"-l", "INFO",
                    "--db", "./public.sqlite",
                    "--json", "long.file.name.here.json"};
    boolean pres = cf.process(arr);
    assertEquals(true, pres) ;

    assertEquals(cf.gotLongOption("help"), false);
    assertEquals(cf.gotLongOption("db"), true);
    assertEquals(cf.gotLongOption("json"), true);

    String[] otherArgs = cf.getArguments();
    assertEquals(otherArgs.length, 0);
  }


  // ==========================================================================
}

package cottonfalcon;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.LinkedList;

public class BasicProcessorTest {

  // ==========================================================================

  @Test
  public void testProcess1s() {

    LinkedList<String> sho = new LinkedList<String>();
    sho.add("s");

    LinkedList<String> lo = new LinkedList<String>(); //
    LinkedList<String> po = new LinkedList<String>();


    BasicProcessor processor = new BasicProcessor() ;
    processor.setShortOptions(sho);
    processor.setLongOptions(lo);
    processor.setParametrisedOptions(po);

    String[] arr = {"-s"};

    boolean result = processor.process(arr);
    if (!result) {
      System.out.println("CF.tst: " + processor.getErrorMessage());
    }

    assertEquals(true, result);
  }

  // ==========================================================================

}
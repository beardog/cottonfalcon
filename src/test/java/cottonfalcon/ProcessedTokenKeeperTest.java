package cottonfalcon;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import java.util.LinkedList;

public class ProcessedTokenKeeperTest {

  // ==========================================================================

  @Test
  public void testLoad3t() {
    ProcessedTokenKeeper ptk = new ProcessedTokenKeeper();

    String[] arr = {"-s", "--ffff", "bar"};

    boolean result = ptk.loadTokens(arr);
    assertEquals(true, result);

    ProcessedToken pt1 = ptk.currentToken();
    assertEquals(pt1.getType(), TokenType.SHORTKEY);
    assertEquals(pt1.getValue(), "s");

    ProcessedToken pt2 = ptk.nextToken();
    assertEquals(pt2.getType(), TokenType.LONGKEY);
    assertEquals(pt2.getValue(), "ffff");

    ProcessedToken pt3 = ptk.nextToken();
    assertEquals(pt3.getType(), TokenType.VALUE);
    assertEquals(pt3.getValue(), "bar");

    assertEquals(false, ptk.hasMoreTokens());
  }

  @Test
  public void testLoad0t() {
    ProcessedTokenKeeper ptk = new ProcessedTokenKeeper();

    String[] arr = {};

    boolean result = ptk.loadTokens(arr);

    assertEquals(true, result);
    assertEquals(false, ptk.hasMoreTokens());
  }

  @Test
  public void testLoad1t() {
    ProcessedTokenKeeper ptk = new ProcessedTokenKeeper();

    String[] arr = {"--ffff"};

    boolean result = ptk.loadTokens(arr);
    assertEquals(true, result);
    assertEquals(false, ptk.hasMoreTokens());

    ProcessedToken pt1 = ptk.currentToken();
    assertEquals(pt1.getType(), TokenType.LONGKEY);
    assertEquals(pt1.getValue(), "ffff");

    ProcessedToken pt2 = ptk.nextToken();
    assertEquals(pt2, null);
    assertEquals(false, ptk.hasMoreTokens());
  }
  // ==========================================================================
}

package cottonfalcon;

import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ProcessedTokenTest {

  // ==========================================================================

  @Test
  public void testConstructorEmpty() {
    ProcessedToken pt = new ProcessedToken("");

    assertEquals(pt.getType(), TokenType.ERROR);
    assertEquals(pt.getValue(), "");
  }

  // ==========================================================================

  @Test
  public void testConstructorShort() {
    ProcessedToken pt = new ProcessedToken("-q");

    assertEquals(pt.getType(), TokenType.SHORTKEY);
    assertEquals(pt.getValue(), "q");
  }

  @Test
  public void testConstructorShortWrong() {
    ProcessedToken pt = new ProcessedToken("-qsss");

    assertEquals(pt.getType(), TokenType.ERROR);
    assertEquals(pt.getValue(), "");
  }

  // ==========================================================================

  @Test
  public void testConstructorLong() {
    ProcessedToken pt = new ProcessedToken("--ddddd");

    assertEquals(pt.getType(), TokenType.LONGKEY);
    assertEquals(pt.getValue(), "ddddd");
  }

  @Test
  public void testConstructorLongWrong() {
    ProcessedToken pt = new ProcessedToken("--q");

    assertEquals(pt.getType(), TokenType.ERROR);
    assertEquals(pt.getValue(), "");
  }


  // ==========================================================================
}